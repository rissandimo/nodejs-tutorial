const http = require('http');

// Call back fn will be run each time a Client connect to server -> send it homepage
// Request - info about the Client and his request
// Response - gateway to communicate with Client
const server = http.createServer((req, res) => {
    // This code is running on the server (backend) - not the front end. So it won't appear in the console
    console.log('client made request to server');

    // Set Response Header Type -> give browser some context/info as to what type of info it will recieve (html, text, JSON, etc..)
    res.setHeader('Content-Type', 'text/plain');
    res.write('this is coming from the server');
    res.end(); // end response -> sends it to browser
});

server.listen(3000, 'localhost', () => {
    console.log("Server running and listening for requests on port: 3000");
})
